//Constants
const players = [
  {
    name: "Paulo",
    nationality: "brazil",
    wins: 2,
    loses: 1,
    ties: 1,
    points: 7
  },
  {
    name: "Rafa",
    nationality: "brazil",
    wins: 1,
    loses: 2,
    ties: 4,
    points: 5
  }
];
const newPlayerNameHtml = document.querySelector("#newPlayerName");
const nationalitySelectorHtml = document.querySelector("#nationalitySelect");
const addButtonHtml = document.querySelector("#addButton");
const removeButtonHtml = document.querySelector("#removeButton");
const inputErrorLabelHtml = document.querySelector("#inputErrorLabel");
const playersTableHtml = document.querySelector("#playersTable");
const chkBoxAllHtml = document.querySelector("#chkBoxAll");

//Functions
const addPlayer = (playerName, nationality) => {
  if (playerName !== "" && !players.find((p) => p.name === playerName && p.nationality === nationality)) {
    inputErrorLabelHtml.innerHTML = "None";
    inputErrorLabelHtml.className = "none";
    const player = {
      name: playerName,
      nationality: nationality,
      wins: 0,
      loses: 0,
      ties: 0,
      points: 0
    };
    players.push(player);
    updateClassificationTable();
  } else if (playerName === "") {
    inputErrorLabelHtml.innerHTML = "Nome Inválido";
    inputErrorLabelHtml.className = "error";
  } else {
    inputErrorLabelHtml.innerHTML = "Jogador já presente na tabela";
    inputErrorLabelHtml.className = "error";
  }
};

const removePlayer = (player) => {
  const playerIndex = players.findIndex(p => p.name === player.name && p.nationality === player.nationality)
  if (playerIndex !== -1) {
    players.splice(playerIndex, 1);
  }
  updateClassificationTable();
};

const calculatePoints = (player) => {
  return player.wins * 3 + player.ties;
};

const buildPlayerTableRow = (player, index) => {
  let html = "<tr>";
  html += '<td> <input type="checkbox" id="chkBox' + index + '" class ="chkBox"> </td>';
  html += '<td>' + player.name + '</td>';
  html += '<td><img src="https://cdn.countryflags.com/thumbs/' + player.nationality + '/flag-round-250.png"></td>'
  html += '<td>' + player.wins + '</td>';
  html += '<td>' + player.ties + '</td>';
  html += '<td>' + player.loses + '</td>';
  html += '<td>' + player.points + '</td>';
  html += '<td><button onClick="addWin(' + index + ')">Vitória</button></td>';
  html += '<td><button onClick="addTie(' + index + ')">Empate</button></td>';
  html += '<td><button onClick="addLose(' + index + ')">Derrota</button></td>';
  html += "</tr>";
  return html;
};

const updateClassificationTable = () => {
  let html = "";
  for (let i = 0; i < players.length; i++) {
    players[i].points = calculatePoints(players[i]);
    html += buildPlayerTableRow(players[i], i);
  }
  playersTableHtml.innerHTML = html;
};

const addWin = (playerIndex) => {
  const player = players[playerIndex];
  player.wins++;
  updateClassificationTable();
};

const addLose = (playerIndex) => {
  const player = players[playerIndex];
  player.loses++;
  updateClassificationTable();
};

const addTie = (playerIndex) => {
  const player = players[playerIndex];
  player.ties++;
  updateClassificationTable();
};

//Code
newPlayerNameHtml.focus();
addButtonHtml.onclick = () => {
  addPlayer(newPlayerNameHtml.value, nationalitySelectorHtml.value);
};
chkBoxAllHtml.onchange = () => {
  document.querySelectorAll(".chkBox").forEach((e) => e.checked = chkBoxAllHtml.checked);
};
removeButtonHtml.onclick = () => {
  const toRemove = [];
  Array.from(document.querySelectorAll(".chkBox")).filter((e) => e.checked == true).forEach((e) => {
    e.checked = false;
    toRemove.push(players[parseInt(e.id.substring(6))]);
  });
  toRemove.forEach(removePlayer);
  chkBoxAllHtml.checked = false;
};